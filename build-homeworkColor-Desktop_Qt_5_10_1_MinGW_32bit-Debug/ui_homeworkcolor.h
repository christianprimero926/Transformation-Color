/********************************************************************************
** Form generated from reading UI file 'homeworkcolor.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HOMEWORKCOLOR_H
#define UI_HOMEWORKCOLOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_homeworkColor
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QFrame *frame;
    QHBoxLayout *horizontalLayout;
    QLineEdit *source;
    QPushButton *loadImage;
    QFrame *frameButtons;
    QGridLayout *gridLayout_10;
    QFrame *frame_14;
    QVBoxLayout *verticalLayout;
    QFrame *frame_4;
    QGridLayout *gridLayout_2;
    QRadioButton *radioButtonRGB_HSV;
    QLabel *label;
    QRadioButton *radioButtonRGB;
    QRadioButton *radioButtonRGB_HSL;
    QFrame *frame_9;
    QGridLayout *gridLayout_3;
    QRadioButton *radioButtonQuantization;
    QLabel *label_6;
    QRadioButton *radioButtonSampling;
    QFrame *frame_10;
    QGridLayout *gridLayout_6;
    QRadioButton *radioButtonMean;
    QLabel *label_4;
    QRadioButton *radioButtonMedian;
    QRadioButton *radioButtonGaussian;
    QFrame *frame_6;
    QGridLayout *gridLayout_4;
    QRadioButton *gradientButton;
    QLabel *label_3;
    QRadioButton *prewittButton;
    QRadioButton *sobelButton;
    QRadioButton *cannyButton;
    QFrame *frame_15;
    QVBoxLayout *verticalLayout_5;
    QFrame *frame_5;
    QGridLayout *gridLayout_5;
    QFrame *frame_7;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_5;
    QRadioButton *radioButtonEqualized;
    QRadioButton *radioButtonHistogram;
    QLabel *label_7;
    QFrame *frame_8;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_2;
    QRadioButton *radioButtonOtsu;
    QRadioButton *radioButtonIsodata;
    QFrame *frame_11;
    QGridLayout *gridLayout_7;
    QFrame *frame_12;
    QGridLayout *gridLayout_8;
    QRadioButton *radioButtonDilation;
    QLabel *label_9;
    QRadioButton *radioButtonErosion;
    QRadioButton *radioButtonOpening;
    QRadioButton *radioButtonClosing;
    QLabel *label_8;
    QFrame *frame_13;
    QGridLayout *gridLayout_9;
    QRadioButton *radioButtonDilationGrayScale;
    QRadioButton *radioButtonErosionGrayScale;
    QLabel *label_10;
    QRadioButton *radioButtonErosionNonFlat;
    QRadioButton *radioButtonGradientGrayScale;
    QRadioButton *radioButtonDilationNonFlat;
    QRadioButton *radioButtonOpeningGray;
    QRadioButton *radioButtonClosingGray;
    QFrame *frame_2;
    QHBoxLayout *horizontalLayout_2;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_2;
    QLabel *labelImagen;

    void setupUi(QMainWindow *homeworkColor)
    {
        if (homeworkColor->objectName().isEmpty())
            homeworkColor->setObjectName(QStringLiteral("homeworkColor"));
        homeworkColor->resize(773, 500);
        centralWidget = new QWidget(homeworkColor);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        frame = new QFrame(centralWidget);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setFrameShape(QFrame::Panel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        source = new QLineEdit(frame);
        source->setObjectName(QStringLiteral("source"));
        source->setReadOnly(true);

        horizontalLayout->addWidget(source);

        loadImage = new QPushButton(frame);
        loadImage->setObjectName(QStringLiteral("loadImage"));
        loadImage->setEnabled(true);
        loadImage->setMouseTracking(false);

        horizontalLayout->addWidget(loadImage);


        gridLayout->addWidget(frame, 0, 0, 1, 2);

        frameButtons = new QFrame(centralWidget);
        frameButtons->setObjectName(QStringLiteral("frameButtons"));
        frameButtons->setEnabled(false);
        frameButtons->setFrameShape(QFrame::Panel);
        frameButtons->setFrameShadow(QFrame::Raised);
        gridLayout_10 = new QGridLayout(frameButtons);
        gridLayout_10->setSpacing(6);
        gridLayout_10->setContentsMargins(11, 11, 11, 11);
        gridLayout_10->setObjectName(QStringLiteral("gridLayout_10"));
        frame_14 = new QFrame(frameButtons);
        frame_14->setObjectName(QStringLiteral("frame_14"));
        frame_14->setFrameShape(QFrame::NoFrame);
        frame_14->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(frame_14);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        frame_4 = new QFrame(frame_14);
        frame_4->setObjectName(QStringLiteral("frame_4"));
        frame_4->setFrameShape(QFrame::Panel);
        frame_4->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(frame_4);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        radioButtonRGB_HSV = new QRadioButton(frame_4);
        radioButtonRGB_HSV->setObjectName(QStringLiteral("radioButtonRGB_HSV"));

        gridLayout_2->addWidget(radioButtonRGB_HSV, 3, 0, 1, 1);

        label = new QLabel(frame_4);
        label->setObjectName(QStringLiteral("label"));
        QFont font;
        font.setPointSize(10);
        label->setFont(font);

        gridLayout_2->addWidget(label, 0, 0, 1, 1);

        radioButtonRGB = new QRadioButton(frame_4);
        radioButtonRGB->setObjectName(QStringLiteral("radioButtonRGB"));

        gridLayout_2->addWidget(radioButtonRGB, 4, 0, 1, 1);

        radioButtonRGB_HSL = new QRadioButton(frame_4);
        radioButtonRGB_HSL->setObjectName(QStringLiteral("radioButtonRGB_HSL"));

        gridLayout_2->addWidget(radioButtonRGB_HSL, 5, 0, 1, 1);


        verticalLayout->addWidget(frame_4);

        frame_9 = new QFrame(frame_14);
        frame_9->setObjectName(QStringLiteral("frame_9"));
        frame_9->setFrameShape(QFrame::Panel);
        frame_9->setFrameShadow(QFrame::Raised);
        gridLayout_3 = new QGridLayout(frame_9);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        radioButtonQuantization = new QRadioButton(frame_9);
        radioButtonQuantization->setObjectName(QStringLiteral("radioButtonQuantization"));

        gridLayout_3->addWidget(radioButtonQuantization, 1, 0, 1, 1);

        label_6 = new QLabel(frame_9);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setFont(font);

        gridLayout_3->addWidget(label_6, 0, 0, 1, 1);

        radioButtonSampling = new QRadioButton(frame_9);
        radioButtonSampling->setObjectName(QStringLiteral("radioButtonSampling"));

        gridLayout_3->addWidget(radioButtonSampling, 2, 0, 1, 1);


        verticalLayout->addWidget(frame_9);

        frame_10 = new QFrame(frame_14);
        frame_10->setObjectName(QStringLiteral("frame_10"));
        frame_10->setFrameShape(QFrame::Panel);
        frame_10->setFrameShadow(QFrame::Raised);
        gridLayout_6 = new QGridLayout(frame_10);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        radioButtonMean = new QRadioButton(frame_10);
        radioButtonMean->setObjectName(QStringLiteral("radioButtonMean"));

        gridLayout_6->addWidget(radioButtonMean, 1, 0, 1, 1);

        label_4 = new QLabel(frame_10);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font);

        gridLayout_6->addWidget(label_4, 0, 0, 1, 1);

        radioButtonMedian = new QRadioButton(frame_10);
        radioButtonMedian->setObjectName(QStringLiteral("radioButtonMedian"));

        gridLayout_6->addWidget(radioButtonMedian, 2, 0, 1, 1);

        radioButtonGaussian = new QRadioButton(frame_10);
        radioButtonGaussian->setObjectName(QStringLiteral("radioButtonGaussian"));

        gridLayout_6->addWidget(radioButtonGaussian, 3, 0, 1, 1);


        verticalLayout->addWidget(frame_10);

        frame_6 = new QFrame(frame_14);
        frame_6->setObjectName(QStringLiteral("frame_6"));
        frame_6->setFrameShape(QFrame::Panel);
        frame_6->setFrameShadow(QFrame::Raised);
        gridLayout_4 = new QGridLayout(frame_6);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        gradientButton = new QRadioButton(frame_6);
        gradientButton->setObjectName(QStringLiteral("gradientButton"));

        gridLayout_4->addWidget(gradientButton, 2, 0, 1, 1);

        label_3 = new QLabel(frame_6);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFont(font);

        gridLayout_4->addWidget(label_3, 0, 0, 2, 2);

        prewittButton = new QRadioButton(frame_6);
        prewittButton->setObjectName(QStringLiteral("prewittButton"));

        gridLayout_4->addWidget(prewittButton, 4, 0, 1, 1);

        sobelButton = new QRadioButton(frame_6);
        sobelButton->setObjectName(QStringLiteral("sobelButton"));

        gridLayout_4->addWidget(sobelButton, 3, 0, 1, 1);

        cannyButton = new QRadioButton(frame_6);
        cannyButton->setObjectName(QStringLiteral("cannyButton"));

        gridLayout_4->addWidget(cannyButton, 5, 0, 1, 1);


        verticalLayout->addWidget(frame_6);


        gridLayout_10->addWidget(frame_14, 0, 0, 1, 1);

        frame_15 = new QFrame(frameButtons);
        frame_15->setObjectName(QStringLiteral("frame_15"));
        frame_15->setFrameShape(QFrame::NoFrame);
        frame_15->setFrameShadow(QFrame::Raised);
        verticalLayout_5 = new QVBoxLayout(frame_15);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        frame_5 = new QFrame(frame_15);
        frame_5->setObjectName(QStringLiteral("frame_5"));
        frame_5->setFrameShape(QFrame::Panel);
        frame_5->setFrameShadow(QFrame::Raised);
        gridLayout_5 = new QGridLayout(frame_5);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        frame_7 = new QFrame(frame_5);
        frame_7->setObjectName(QStringLiteral("frame_7"));
        frame_7->setFrameShape(QFrame::Panel);
        frame_7->setFrameShadow(QFrame::Raised);
        verticalLayout_4 = new QVBoxLayout(frame_7);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        label_5 = new QLabel(frame_7);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setFont(font);

        verticalLayout_4->addWidget(label_5);

        radioButtonEqualized = new QRadioButton(frame_7);
        radioButtonEqualized->setObjectName(QStringLiteral("radioButtonEqualized"));

        verticalLayout_4->addWidget(radioButtonEqualized);

        radioButtonHistogram = new QRadioButton(frame_7);
        radioButtonHistogram->setObjectName(QStringLiteral("radioButtonHistogram"));

        verticalLayout_4->addWidget(radioButtonHistogram);


        gridLayout_5->addWidget(frame_7, 1, 0, 1, 1);

        label_7 = new QLabel(frame_5);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setFont(font);

        gridLayout_5->addWidget(label_7, 0, 0, 1, 1);

        frame_8 = new QFrame(frame_5);
        frame_8->setObjectName(QStringLiteral("frame_8"));
        frame_8->setFrameShape(QFrame::Panel);
        frame_8->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame_8);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        label_2 = new QLabel(frame_8);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font);

        verticalLayout_3->addWidget(label_2);

        radioButtonOtsu = new QRadioButton(frame_8);
        radioButtonOtsu->setObjectName(QStringLiteral("radioButtonOtsu"));

        verticalLayout_3->addWidget(radioButtonOtsu);

        radioButtonIsodata = new QRadioButton(frame_8);
        radioButtonIsodata->setObjectName(QStringLiteral("radioButtonIsodata"));

        verticalLayout_3->addWidget(radioButtonIsodata);


        gridLayout_5->addWidget(frame_8, 2, 0, 1, 1);


        verticalLayout_5->addWidget(frame_5);

        frame_11 = new QFrame(frame_15);
        frame_11->setObjectName(QStringLiteral("frame_11"));
        frame_11->setFrameShape(QFrame::Panel);
        frame_11->setFrameShadow(QFrame::Raised);
        gridLayout_7 = new QGridLayout(frame_11);
        gridLayout_7->setSpacing(6);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        frame_12 = new QFrame(frame_11);
        frame_12->setObjectName(QStringLiteral("frame_12"));
        frame_12->setFrameShape(QFrame::Panel);
        frame_12->setFrameShadow(QFrame::Raised);
        gridLayout_8 = new QGridLayout(frame_12);
        gridLayout_8->setSpacing(6);
        gridLayout_8->setContentsMargins(11, 11, 11, 11);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        radioButtonDilation = new QRadioButton(frame_12);
        radioButtonDilation->setObjectName(QStringLiteral("radioButtonDilation"));

        gridLayout_8->addWidget(radioButtonDilation, 3, 0, 1, 1);

        label_9 = new QLabel(frame_12);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setFont(font);

        gridLayout_8->addWidget(label_9, 0, 0, 1, 1);

        radioButtonErosion = new QRadioButton(frame_12);
        radioButtonErosion->setObjectName(QStringLiteral("radioButtonErosion"));

        gridLayout_8->addWidget(radioButtonErosion, 2, 0, 1, 1);

        radioButtonOpening = new QRadioButton(frame_12);
        radioButtonOpening->setObjectName(QStringLiteral("radioButtonOpening"));

        gridLayout_8->addWidget(radioButtonOpening, 2, 1, 1, 1);

        radioButtonClosing = new QRadioButton(frame_12);
        radioButtonClosing->setObjectName(QStringLiteral("radioButtonClosing"));

        gridLayout_8->addWidget(radioButtonClosing, 3, 1, 1, 1);


        gridLayout_7->addWidget(frame_12, 1, 0, 1, 1);

        label_8 = new QLabel(frame_11);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setFont(font);

        gridLayout_7->addWidget(label_8, 0, 0, 1, 1);

        frame_13 = new QFrame(frame_11);
        frame_13->setObjectName(QStringLiteral("frame_13"));
        frame_13->setFrameShape(QFrame::Panel);
        frame_13->setFrameShadow(QFrame::Raised);
        gridLayout_9 = new QGridLayout(frame_13);
        gridLayout_9->setSpacing(6);
        gridLayout_9->setContentsMargins(11, 11, 11, 11);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        radioButtonDilationGrayScale = new QRadioButton(frame_13);
        radioButtonDilationGrayScale->setObjectName(QStringLiteral("radioButtonDilationGrayScale"));

        gridLayout_9->addWidget(radioButtonDilationGrayScale, 2, 0, 1, 1);

        radioButtonErosionGrayScale = new QRadioButton(frame_13);
        radioButtonErosionGrayScale->setObjectName(QStringLiteral("radioButtonErosionGrayScale"));

        gridLayout_9->addWidget(radioButtonErosionGrayScale, 1, 0, 1, 1);

        label_10 = new QLabel(frame_13);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setFont(font);

        gridLayout_9->addWidget(label_10, 0, 0, 1, 1);

        radioButtonErosionNonFlat = new QRadioButton(frame_13);
        radioButtonErosionNonFlat->setObjectName(QStringLiteral("radioButtonErosionNonFlat"));

        gridLayout_9->addWidget(radioButtonErosionNonFlat, 4, 0, 1, 1);

        radioButtonGradientGrayScale = new QRadioButton(frame_13);
        radioButtonGradientGrayScale->setObjectName(QStringLiteral("radioButtonGradientGrayScale"));

        gridLayout_9->addWidget(radioButtonGradientGrayScale, 1, 1, 1, 1);

        radioButtonDilationNonFlat = new QRadioButton(frame_13);
        radioButtonDilationNonFlat->setObjectName(QStringLiteral("radioButtonDilationNonFlat"));

        gridLayout_9->addWidget(radioButtonDilationNonFlat, 3, 0, 1, 1);

        radioButtonOpeningGray = new QRadioButton(frame_13);
        radioButtonOpeningGray->setObjectName(QStringLiteral("radioButtonOpeningGray"));

        gridLayout_9->addWidget(radioButtonOpeningGray, 2, 1, 1, 1);

        radioButtonClosingGray = new QRadioButton(frame_13);
        radioButtonClosingGray->setObjectName(QStringLiteral("radioButtonClosingGray"));

        gridLayout_9->addWidget(radioButtonClosingGray, 3, 1, 1, 1);


        gridLayout_7->addWidget(frame_13, 2, 0, 1, 1);


        verticalLayout_5->addWidget(frame_11);


        gridLayout_10->addWidget(frame_15, 0, 1, 1, 1);


        gridLayout->addWidget(frameButtons, 1, 1, 1, 1);

        frame_2 = new QFrame(centralWidget);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setFrameShape(QFrame::Panel);
        frame_2->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame_2);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        scrollArea = new QScrollArea(frame_2);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setFrameShape(QFrame::StyledPanel);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 410, 432));
        verticalLayout_2 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        labelImagen = new QLabel(scrollAreaWidgetContents);
        labelImagen->setObjectName(QStringLiteral("labelImagen"));
        labelImagen->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(labelImagen);

        scrollArea->setWidget(scrollAreaWidgetContents);

        horizontalLayout_2->addWidget(scrollArea);


        gridLayout->addWidget(frame_2, 1, 0, 1, 1);

        homeworkColor->setCentralWidget(centralWidget);

        retranslateUi(homeworkColor);

        QMetaObject::connectSlotsByName(homeworkColor);
    } // setupUi

    void retranslateUi(QMainWindow *homeworkColor)
    {
        homeworkColor->setWindowTitle(QApplication::translate("homeworkColor", "Image Processing Application", nullptr));
        source->setText(QApplication::translate("homeworkColor", "Path Image", nullptr));
        loadImage->setText(QApplication::translate("homeworkColor", "Load Image", nullptr));
        radioButtonRGB_HSV->setText(QApplication::translate("homeworkColor", "RGB --> HSV", nullptr));
        label->setText(QApplication::translate("homeworkColor", "TransFormation", nullptr));
        radioButtonRGB->setText(QApplication::translate("homeworkColor", "RGB --> RGB", nullptr));
        radioButtonRGB_HSL->setText(QApplication::translate("homeworkColor", "RGB --> HSL", nullptr));
        radioButtonQuantization->setText(QApplication::translate("homeworkColor", "Quantization", nullptr));
        label_6->setText(QApplication::translate("homeworkColor", "Sampling & Quantization", nullptr));
        radioButtonSampling->setText(QApplication::translate("homeworkColor", "Sampling", nullptr));
        radioButtonMean->setText(QApplication::translate("homeworkColor", "Mean Filter", nullptr));
        label_4->setText(QApplication::translate("homeworkColor", "Smoothing", nullptr));
        radioButtonMedian->setText(QApplication::translate("homeworkColor", "Median Filter", nullptr));
        radioButtonGaussian->setText(QApplication::translate("homeworkColor", "Gaussian Filter", nullptr));
        gradientButton->setText(QApplication::translate("homeworkColor", "Gradient", nullptr));
        label_3->setText(QApplication::translate("homeworkColor", "Edge Detection", nullptr));
        prewittButton->setText(QApplication::translate("homeworkColor", "Prewitt", nullptr));
        sobelButton->setText(QApplication::translate("homeworkColor", "Sobel", nullptr));
        cannyButton->setText(QApplication::translate("homeworkColor", "Canny", nullptr));
        label_5->setText(QApplication::translate("homeworkColor", "Histogram", nullptr));
        radioButtonEqualized->setText(QApplication::translate("homeworkColor", "Image Equalized -- Histogram", nullptr));
        radioButtonHistogram->setText(QApplication::translate("homeworkColor", "Generate Histogram", nullptr));
        label_7->setText(QApplication::translate("homeworkColor", "Histogram & Thresholding", nullptr));
        label_2->setText(QApplication::translate("homeworkColor", "Thresholding", nullptr));
        radioButtonOtsu->setText(QApplication::translate("homeworkColor", "Otsu's Method", nullptr));
        radioButtonIsodata->setText(QApplication::translate("homeworkColor", "Isodata's Method", nullptr));
        radioButtonDilation->setText(QApplication::translate("homeworkColor", "Dilation", nullptr));
        label_9->setText(QApplication::translate("homeworkColor", "Binary", nullptr));
        radioButtonErosion->setText(QApplication::translate("homeworkColor", "Erosion", nullptr));
        radioButtonOpening->setText(QApplication::translate("homeworkColor", "Opening", nullptr));
        radioButtonClosing->setText(QApplication::translate("homeworkColor", "Closing", nullptr));
        label_8->setText(QApplication::translate("homeworkColor", "Morphological Operators", nullptr));
        radioButtonDilationGrayScale->setText(QApplication::translate("homeworkColor", "Dilation (Flat)", nullptr));
        radioButtonErosionGrayScale->setText(QApplication::translate("homeworkColor", "Erosion (Flat)", nullptr));
        label_10->setText(QApplication::translate("homeworkColor", "GrayScale", nullptr));
        radioButtonErosionNonFlat->setText(QApplication::translate("homeworkColor", "Erosion (Non-Flat)", nullptr));
        radioButtonGradientGrayScale->setText(QApplication::translate("homeworkColor", "Gradient", nullptr));
        radioButtonDilationNonFlat->setText(QApplication::translate("homeworkColor", "Dilation (Non-Flat)", nullptr));
        radioButtonOpeningGray->setText(QApplication::translate("homeworkColor", "Opening", nullptr));
        radioButtonClosingGray->setText(QApplication::translate("homeworkColor", "Closing", nullptr));
        labelImagen->setText(QApplication::translate("homeworkColor", "Imagen", nullptr));
    } // retranslateUi

};

namespace Ui {
    class homeworkColor: public Ui_homeworkColor {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HOMEWORKCOLOR_H
