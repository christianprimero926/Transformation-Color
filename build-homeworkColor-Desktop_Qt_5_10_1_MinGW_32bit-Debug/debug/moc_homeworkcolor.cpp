/****************************************************************************
** Meta object code from reading C++ file 'homeworkcolor.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "F:/Universidad/Univalle-Ingenieria_en_sistemas/2018_7_Semestre/Electiva_Profesional_II-Introduccion_al_procesamiento_digital_de_imagenes/Tareas/Tarea1/Version Final/homeworkColor/homeworkcolor.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'homeworkcolor.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_homeworkColor_t {
    QByteArrayData data[30];
    char stringdata0[876];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_homeworkColor_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_homeworkColor_t qt_meta_stringdata_homeworkColor = {
    {
QT_MOC_LITERAL(0, 0, 13), // "homeworkColor"
QT_MOC_LITERAL(1, 14, 20), // "on_loadImage_clicked"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 29), // "on_radioButtonRGB_HSL_clicked"
QT_MOC_LITERAL(4, 66, 29), // "on_radioButtonRGB_HSV_clicked"
QT_MOC_LITERAL(5, 96, 25), // "on_radioButtonRGB_clicked"
QT_MOC_LITERAL(6, 122, 26), // "on_radioButtonOtsu_clicked"
QT_MOC_LITERAL(7, 149, 29), // "on_radioButtonIsodata_clicked"
QT_MOC_LITERAL(8, 179, 31), // "on_radioButtonEqualized_clicked"
QT_MOC_LITERAL(9, 211, 31), // "on_radioButtonHistogram_clicked"
QT_MOC_LITERAL(10, 243, 26), // "on_radioButtonMean_clicked"
QT_MOC_LITERAL(11, 270, 28), // "on_radioButtonMedian_clicked"
QT_MOC_LITERAL(12, 299, 30), // "on_radioButtonGaussian_clicked"
QT_MOC_LITERAL(13, 330, 30), // "on_radioButtonSampling_clicked"
QT_MOC_LITERAL(14, 361, 34), // "on_radioButtonQuantization_cl..."
QT_MOC_LITERAL(15, 396, 25), // "on_gradientButton_clicked"
QT_MOC_LITERAL(16, 422, 22), // "on_sobelButton_clicked"
QT_MOC_LITERAL(17, 445, 24), // "on_prewittButton_clicked"
QT_MOC_LITERAL(18, 470, 29), // "on_radioButtonErosion_clicked"
QT_MOC_LITERAL(19, 500, 30), // "on_radioButtonDilation_clicked"
QT_MOC_LITERAL(20, 531, 38), // "on_radioButtonErosionGrayScal..."
QT_MOC_LITERAL(21, 570, 39), // "on_radioButtonDilationGraySca..."
QT_MOC_LITERAL(22, 610, 36), // "on_radioButtonErosionNonFlat_..."
QT_MOC_LITERAL(23, 647, 37), // "on_radioButtonDilationNonFlat..."
QT_MOC_LITERAL(24, 685, 39), // "on_radioButtonGradientGraySca..."
QT_MOC_LITERAL(25, 725, 22), // "on_cannyButton_clicked"
QT_MOC_LITERAL(26, 748, 29), // "on_radioButtonOpening_clicked"
QT_MOC_LITERAL(27, 778, 29), // "on_radioButtonClosing_clicked"
QT_MOC_LITERAL(28, 808, 33), // "on_radioButtonOpeningGray_cli..."
QT_MOC_LITERAL(29, 842, 33) // "on_radioButtonClosingGray_cli..."

    },
    "homeworkColor\0on_loadImage_clicked\0\0"
    "on_radioButtonRGB_HSL_clicked\0"
    "on_radioButtonRGB_HSV_clicked\0"
    "on_radioButtonRGB_clicked\0"
    "on_radioButtonOtsu_clicked\0"
    "on_radioButtonIsodata_clicked\0"
    "on_radioButtonEqualized_clicked\0"
    "on_radioButtonHistogram_clicked\0"
    "on_radioButtonMean_clicked\0"
    "on_radioButtonMedian_clicked\0"
    "on_radioButtonGaussian_clicked\0"
    "on_radioButtonSampling_clicked\0"
    "on_radioButtonQuantization_clicked\0"
    "on_gradientButton_clicked\0"
    "on_sobelButton_clicked\0on_prewittButton_clicked\0"
    "on_radioButtonErosion_clicked\0"
    "on_radioButtonDilation_clicked\0"
    "on_radioButtonErosionGrayScale_clicked\0"
    "on_radioButtonDilationGrayScale_clicked\0"
    "on_radioButtonErosionNonFlat_clicked\0"
    "on_radioButtonDilationNonFlat_clicked\0"
    "on_radioButtonGradientGrayScale_clicked\0"
    "on_cannyButton_clicked\0"
    "on_radioButtonOpening_clicked\0"
    "on_radioButtonClosing_clicked\0"
    "on_radioButtonOpeningGray_clicked\0"
    "on_radioButtonClosingGray_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_homeworkColor[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      28,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  154,    2, 0x08 /* Private */,
       3,    0,  155,    2, 0x08 /* Private */,
       4,    0,  156,    2, 0x08 /* Private */,
       5,    0,  157,    2, 0x08 /* Private */,
       6,    0,  158,    2, 0x08 /* Private */,
       7,    0,  159,    2, 0x08 /* Private */,
       8,    0,  160,    2, 0x08 /* Private */,
       9,    0,  161,    2, 0x08 /* Private */,
      10,    0,  162,    2, 0x08 /* Private */,
      11,    0,  163,    2, 0x08 /* Private */,
      12,    0,  164,    2, 0x08 /* Private */,
      13,    0,  165,    2, 0x08 /* Private */,
      14,    0,  166,    2, 0x08 /* Private */,
      15,    0,  167,    2, 0x08 /* Private */,
      16,    0,  168,    2, 0x08 /* Private */,
      17,    0,  169,    2, 0x08 /* Private */,
      18,    0,  170,    2, 0x08 /* Private */,
      19,    0,  171,    2, 0x08 /* Private */,
      20,    0,  172,    2, 0x08 /* Private */,
      21,    0,  173,    2, 0x08 /* Private */,
      22,    0,  174,    2, 0x08 /* Private */,
      23,    0,  175,    2, 0x08 /* Private */,
      24,    0,  176,    2, 0x08 /* Private */,
      25,    0,  177,    2, 0x08 /* Private */,
      26,    0,  178,    2, 0x08 /* Private */,
      27,    0,  179,    2, 0x08 /* Private */,
      28,    0,  180,    2, 0x08 /* Private */,
      29,    0,  181,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void homeworkColor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        homeworkColor *_t = static_cast<homeworkColor *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_loadImage_clicked(); break;
        case 1: _t->on_radioButtonRGB_HSL_clicked(); break;
        case 2: _t->on_radioButtonRGB_HSV_clicked(); break;
        case 3: _t->on_radioButtonRGB_clicked(); break;
        case 4: _t->on_radioButtonOtsu_clicked(); break;
        case 5: _t->on_radioButtonIsodata_clicked(); break;
        case 6: _t->on_radioButtonEqualized_clicked(); break;
        case 7: _t->on_radioButtonHistogram_clicked(); break;
        case 8: _t->on_radioButtonMean_clicked(); break;
        case 9: _t->on_radioButtonMedian_clicked(); break;
        case 10: _t->on_radioButtonGaussian_clicked(); break;
        case 11: _t->on_radioButtonSampling_clicked(); break;
        case 12: _t->on_radioButtonQuantization_clicked(); break;
        case 13: _t->on_gradientButton_clicked(); break;
        case 14: _t->on_sobelButton_clicked(); break;
        case 15: _t->on_prewittButton_clicked(); break;
        case 16: _t->on_radioButtonErosion_clicked(); break;
        case 17: _t->on_radioButtonDilation_clicked(); break;
        case 18: _t->on_radioButtonErosionGrayScale_clicked(); break;
        case 19: _t->on_radioButtonDilationGrayScale_clicked(); break;
        case 20: _t->on_radioButtonErosionNonFlat_clicked(); break;
        case 21: _t->on_radioButtonDilationNonFlat_clicked(); break;
        case 22: _t->on_radioButtonGradientGrayScale_clicked(); break;
        case 23: _t->on_cannyButton_clicked(); break;
        case 24: _t->on_radioButtonOpening_clicked(); break;
        case 25: _t->on_radioButtonClosing_clicked(); break;
        case 26: _t->on_radioButtonOpeningGray_clicked(); break;
        case 27: _t->on_radioButtonClosingGray_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject homeworkColor::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_homeworkColor.data,
      qt_meta_data_homeworkColor,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *homeworkColor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *homeworkColor::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_homeworkColor.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int homeworkColor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 28)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 28;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 28)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 28;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
