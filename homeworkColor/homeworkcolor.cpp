#include "homeworkcolor.h"

using namespace QtCharts;


homeworkColor::homeworkColor(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::homeworkColor)

{
    ui->setupUi(this);
}

homeworkColor::~homeworkColor()
{
    delete ui;
}

void homeworkColor::on_loadImage_clicked(){

    filename = QFileDialog::getOpenFileName(
                this,
                tr("Open File"),
                "C://",
                "Image Files(*.jpg);;All files (*.*)"
                );
    ui->labelImagen->setPixmap(filename);
    ui->source->setText(filename);
    ui->frameButtons->setEnabled(true);
}

void homeworkColor::on_radioButtonRGB_HSL_clicked(){

    convert* reader = new convert;

    reader->loadImage(filename);
    QImage image = reader->getQOriginalImage();

    reader->RGB_HSL(image);
    QImage imageHsl;
    imageHsl = reader->getImageHSL();
    ui->labelImagen->setPixmap(QPixmap::fromImage(imageHsl));
    reader->showImage(reader->getQOriginalImageRed());
    reader->showImage(reader->getQOriginalImageGreen());
    reader->showImage(reader->getQOriginalImageBlue());
}

void homeworkColor::on_radioButtonRGB_HSV_clicked(){
    convert* reader = new convert;

    reader->loadImage(filename);
    QImage image = reader->getQOriginalImage();
    reader->RGB_HSV(image);
    QImage imageHsv;
    imageHsv = reader->getImageHSV();    
    ui->labelImagen->setPixmap(QPixmap::fromImage(imageHsv));
    reader->showImage(reader->getQOriginalImageRed());
    reader->showImage(reader->getQOriginalImageGreen());
    reader->showImage(reader->getQOriginalImageBlue());
}

void homeworkColor::on_radioButtonRGB_clicked(){
    convert* reader = new convert;
    reader->loadImage(filename);
    QImage image = reader->getQOriginalImage();

    ui->labelImagen->setPixmap(QPixmap::fromImage(image));

    reader->channelsRGB(image);
    reader->showImage(reader->getQOriginalImageRed());
    reader->showImage(reader->getQOriginalImageGreen());
    reader->showImage(reader->getQOriginalImageBlue());

}

void homeworkColor::on_radioButtonOtsu_clicked(){
    convert* histogram = new convert();
    histogram->loadImage(filename);
    int thres = histogram->calculatedThreshold(OTSU);
    histogram->thresholding(thres);
    QImage thresholdedImage;
    thresholdedImage = histogram->getQThresholdedImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(thresholdedImage));
}

void homeworkColor::on_radioButtonIsodata_clicked(){
    convert* histogram = new convert();
    histogram->loadImage(filename);
    int thres = histogram->calculatedThreshold(ISODATA);
    histogram->thresholding(thres);
    QImage thresholdedImage;
    thresholdedImage = histogram->getQThresholdedImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(thresholdedImage));

}

void homeworkColor::on_radioButtonEqualized_clicked(){
      convert* histogram = new convert();
      histogram->loadImage(filename);
      histogram->calculateHistogram();
      histogram->equalizeHistogram();

      QImage equalizedImage = histogram->getQEqualizedImage();
      ui->labelImagen->setPixmap(QPixmap::fromImage(equalizedImage));

      int *equalizedHistogram = histogram->getEqualizedHistogram();
      histogram->showHistogram(equalizedHistogram);
}

void homeworkColor::on_radioButtonHistogram_clicked(){
    convert* histogram = new convert();
    histogram->loadImage(filename);
    histogram->calculateHistogram();

    QImage image = histogram->getQOriginalImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(image));

    int *histogramImage = histogram->getHistogram();
    histogram->showHistogram(histogramImage);
}

void homeworkColor::on_radioButtonMean_clicked(){
    convert* reader = new convert;
    reader->loadImage(filename);
    reader->smothing(MEAN);
    QImage image = reader->getDenoisingImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(image));

}

void homeworkColor::on_radioButtonMedian_clicked(){
    convert* reader = new convert;
    reader->loadImage(filename);
    reader->smothing(MEDIAN);
    QImage image = reader->getDenoisingImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(image));
}

void homeworkColor::on_radioButtonGaussian_clicked(){
    convert* reader = new convert;
    reader->loadImage(filename);
    reader->smothing(GAUSSIAN);
    QImage image = reader->getDenoisingImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(image));
}

void homeworkColor::on_radioButtonSampling_clicked(){
    convert* reader = new convert;
    reader->loadImage(filename);
    reader->bitDepthReduction(2);
    QImage image = reader->getqBitDepthReducedImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(image));
}

void homeworkColor::on_radioButtonQuantization_clicked(){
    convert* reader = new convert;
    reader->loadImage(filename);
    reader->subSumpling(2);
    QImage image = reader->getSampledImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(image));
}

void homeworkColor::on_gradientButton_clicked()
{
    convert* reader = new convert;
    reader->loadImage(filename);
    int extend = 2;
    reader->mirroring(extend);
    reader->smoothing(extend);
    reader->edgeDetection(BASIC, extend);
    QImage edgeImage = reader->getEdgeImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(edgeImage));
}

void homeworkColor::on_sobelButton_clicked()
{
    convert* reader = new convert;
    reader->loadImage(filename);
    int extend = 2;
    reader->mirroring(extend);
    reader->smoothing(extend);
    reader->edgeDetection(SOBEL, extend);
    QImage edgeImage = reader->getEdgeImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(edgeImage));

}

void homeworkColor::on_prewittButton_clicked()
{
    convert* reader = new convert;
    reader->loadImage(filename);
    int extend = 2;
    reader->mirroring(extend);
    reader->smoothing(extend);
    reader->edgeDetection(PREWITT, extend);
    QImage edgeImage = reader->getEdgeImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(edgeImage));
}

void homeworkColor::on_radioButtonErosion_clicked()
{
    convert* reader = new convert;
    reader->loadImage(filename);
    reader->thresholding(127);
    reader->binaryErosion(EROSION);
    QImage morphologicalImage = reader->getQMorphologicalImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(morphologicalImage));
}

void homeworkColor::on_radioButtonDilation_clicked()
{
    convert* reader = new convert;
    reader->loadImage(filename);
    reader->thresholding(127);
    reader->binaryErosion(DILATION);
    QImage morphologicalImage = reader->getQMorphologicalImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(morphologicalImage));
}

void homeworkColor::on_radioButtonErosionGrayScale_clicked()
{
    convert* reader = new convert;
    reader->loadImage(filename);
    reader->binaryErosion(GRAYF_EROSION);
    QImage morphologicalImage = reader->getQMorphologicalImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(morphologicalImage));
}

void homeworkColor::on_radioButtonDilationGrayScale_clicked()
{
    convert* reader = new convert;
    reader->loadImage(filename);
    reader->binaryErosion(GRAYF_DILATION);
    QImage morphologicalImage = reader->getQMorphologicalImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(morphologicalImage));
}

void homeworkColor::on_radioButtonErosionNonFlat_clicked()
{
    convert* reader = new convert;
    reader->loadImage(filename);
    reader->binaryErosion(GRAYNF_EROSION);
    QImage morphologicalImage = reader->getQMorphologicalImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(morphologicalImage));
}

void homeworkColor::on_radioButtonDilationNonFlat_clicked()
{
    convert* reader = new convert;
    reader->loadImage(filename);
    reader->binaryErosion(GRAYNF_DILATION);
    QImage morphologicalImage = reader->getQMorphologicalImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(morphologicalImage));
}

void homeworkColor::on_radioButtonGradientGrayScale_clicked()
{
    convert* reader = new convert;
    reader->loadImage(filename);
    reader->binaryErosion(GRADIENT);
    QImage morphologicalImage = reader->getQMorphologicalImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(morphologicalImage));
}

void homeworkColor::on_cannyButton_clicked()
{
    convert* reader = new convert;
    reader->loadImage(filename);
    int extend = 2;
    reader->mirroring(extend);    
    reader->edgeDetection(CANNY, extend);
    QImage edgeImage = reader->getQNonMaximaImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(edgeImage));
}

void homeworkColor::on_radioButtonOpening_clicked()
{
    QFile image("imageDilation.jpg");
    image.remove();
    convert* reader = new convert;
    reader->loadImage(filename);
    reader->thresholding(127);
    reader->binaryErosion(DILATION);
    QImage morphologicalErosion = reader->getQMorphologicalImage();
    reader->writeImage(morphologicalErosion,"imageDilation.jpg");

    reader->loadImage("imageDilation.jpg");
    reader->thresholding(127);
    reader->binaryErosion(EROSION);
    QImage morphologicalImage = reader->getQMorphologicalImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(morphologicalImage));
}

void homeworkColor::on_radioButtonClosing_clicked()
{
    QFile image("imageErosion.jpg");
    image.remove();
    convert* reader = new convert;
    reader->loadImage(filename);
    reader->thresholding(127);
    reader->binaryErosion(EROSION);
    QImage morphologicalErosion = reader->getQMorphologicalImage();
    reader->writeImage(morphologicalErosion,"imageErosion.jpg");

    reader->loadImage("imageErosion.jpg");
    reader->thresholding(127);
    reader->binaryErosion(DILATION);
    QImage morphologicalImage = reader->getQMorphologicalImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(morphologicalImage));
}

void homeworkColor::on_radioButtonOpeningGray_clicked()
{
    QFile image("imageDilationFlat.jpg");
    image.remove();
    convert* reader = new convert;
    reader->loadImage(filename);
    reader->thresholding(127);
    reader->binaryErosion(GRAYF_DILATION);
    QImage morphologicalErosion = reader->getQMorphologicalImage();
    reader->writeImage(morphologicalErosion,"imageDilationFlat.jpg");

    reader->loadImage("imageDilationFlat.jpg");
    reader->thresholding(127);
    reader->binaryErosion(GRAYF_EROSION);
    QImage morphologicalImage = reader->getQMorphologicalImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(morphologicalImage));
}

void homeworkColor::on_radioButtonClosingGray_clicked()
{
    QFile image("imageErosionFlat.jpg");
    image.remove();
    convert* reader = new convert;
    reader->loadImage(filename);
    reader->thresholding(127);
    reader->binaryErosion(GRAYF_EROSION);
    QImage morphologicalErosion = reader->getQMorphologicalImage();
    reader->writeImage(morphologicalErosion,"imageErosionFlat.jpg");

    reader->loadImage("imageErosionFlat.jpg");
    reader->thresholding(127);
    reader->binaryErosion(GRAYF_DILATION);
    QImage morphologicalImage = reader->getQMorphologicalImage();
    ui->labelImagen->setPixmap(QPixmap::fromImage(morphologicalImage));
}
