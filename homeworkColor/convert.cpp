#include "convert.h"

using namespace QtCharts;

int gaussian3x3 [3][3] = {{1, 2, 1},
                          {2, 4, 2},
                          {1, 2, 1}};

int gaussian5x5 [5][5] = {{2, 4, 5, 4, 2},
                          {4, 9, 12, 9, 4},
                          {5, 12, 15, 12, 5},
                          {4, 9, 12, 9, 4},
                          {2, 4, 5, 4, 2}};
struct element{
    int kernel[49] = {0,0,1,1,1,0,0,
                      0,1,1,1,1,1,0,
                      1,1,1,1,1,1,1,
                      1,1,1,1,1,1,1,
                      1,1,1,1,1,1,1,
                      0,1,1,1,1,1,0,
                      0,0,1,1,1,0,0};
    int width = 7;
    int height = 7;
    int origen_x = 3;
    int origen_y = 3;
};

convert::convert()
{

}
QImage convert::getQOriginalImage(){
    return qOriginalImage;
}
QImage convert::getDenoisingImage(){
    return qDenoisingImage;
}
QImage convert::getImageHSL(){
    return imageHSL;
}
QImage convert::getImageHSV(){
    return imageHSV;
}
QImage convert::getQOriginalImageRed(){
    return imageRed;
}
QImage convert::getQOriginalImageGreen(){
    return imageGreen;
}
QImage convert::getQOriginalImageBlue(){
    return imageBlue;
}
QImage convert::getSampledImage(){
    return QSampledImage;
}
QImage convert::getqBitDepthReducedImage(){
    return qBitDepthReducedImage;
}
QImage convert::getQEqualizedImage(){
    return qEqualizedImage;
}
QImage convert::getQThresholdedImage(){
    return qTresHoldedImage;
}
QImage convert::getMirroringImage(){
    return qMirroringImage;
}
QImage convert::getGradientImage(){
    return qGradientImage;
}
QImage convert::getGradientXImage(){
    return qGradientXImage;
}
QImage convert::getGradientYImage(){
    return qGradientYImage;
}
QImage convert::getEdgeImage(){
    return qEdgeImage;
}
QImage convert::getqCalculateGradientImage(){
    return qCalculateGradientImage;
}
QImage convert::getQMorphologicalImage(){
    return qMorphologicalImage;
}
QImage convert::getQNonMaximaImage(){
    return qNonMaximaImage;
}
int* convert::getHistogram(){
    return histogram;
}
int* convert::getAccumulatedFrequency(){
    return accumulatedFrequency;
}
int* convert::getEqualizedHistogram(){
    return equalizedHistogram;
}

void convert::loadImage(QString filename){
    QImageReader reader(filename);
    qOriginalImage = reader.read();

    if(qOriginalImage.isNull()){
        printf("Invalid Image \n");
        exit(EXIT_FAILURE);
    }
    width = qOriginalImage.width();
    height = qOriginalImage.height();
    bitDepth = qOriginalImage.depth();
    format = qOriginalImage.format();
    printf("image. Size: %d x %d to %d bits with format %d\n",
           width, height, bitDepth, format);

    originalPixels = qOriginalImage.bits();

}
void convert::writeImage(QImage imageToSave, QString imageNameOut){
    /**Create Image Read object**/
    QImageWriter writer(imageNameOut);

    /**Read Image on device and load QImage object**/
    writer.write(imageToSave);
}
void convert::showImage(QImage image){
    scene = new QGraphicsScene();
    view = new QGraphicsView(scene);
    item = new QGraphicsPixmapItem(QPixmap::fromImage(image));
    scene->addItem(item);
    view->show();
}
void convert::mirroring(int extend){
    qMirroringImage = QImage(width + (extend * 2), height + (extend * 2),  QImage::Format_Grayscale8);
    mirroringPixels = qMirroringImage.bits();

    for(int y = 0; y < height; y++)
        for(int x = 0; x < width; x++){
            qMirroringImage.setPixel(x + extend, y + extend, qOriginalImage.pixel(x,y));
    }

}

void convert::edgeDetection(Algorithms edge, int extend){

    qEdgeImage =  QImage(width, height, QImage::Format_Grayscale8);

    edgePixels = qEdgeImage.bits();

    if(edge == BASIC){
        for(int y =0; y < height; y++)
            for(int x = 0; x < width; x++){
                int value_x = 0;
                int value_y = 0;
                value_x -= qMirroringImage.pixelColor((x + extend), (y + extend)).value();
                value_x += qMirroringImage.pixelColor((x + extend) + 1, (y + extend)).value();

                value_y += qMirroringImage.pixelColor((x + extend), (y + extend)).value();
                value_y -= qMirroringImage.pixelColor((x + extend), (y + extend) + 1).value();

                int gradient = qAbs(value_x) + qAbs(value_y);
                edgePixels[x + (y * width)] =  gradient > 255 ? 255: gradient ;

            }
    }

    if(edge == PREWITT){
        for(int y =0; y < height; y++)
            for(int x = 0; x < width; x++){
                int value_x = 0;
                int value_y = 0;

                /// Calculate Gradient X
                value_x += qMirroringImage.pixelColor((x + extend) - 1, (y + extend) - 1).value();
                value_x -= qMirroringImage.pixelColor((x + extend) + 1, (y + extend) - 1).value();
                value_x += qMirroringImage.pixelColor((x + extend) - 1, (y + extend)).value();
                value_x -= qMirroringImage.pixelColor((x + extend) + 1, (y + extend)).value();
                value_x += qMirroringImage.pixelColor((x + extend) - 1, (y + extend) + 1).value();
                value_x -= qMirroringImage.pixelColor((x + extend) + 1, (y + extend) + 1).value();

                /// Calculate Gradient Y
                value_y -= qMirroringImage.pixelColor((x + extend) - 1, (y + extend) - 1).value();
                value_y -= qMirroringImage.pixelColor((x + extend), (y + extend) - 1).value();
                value_y -= qMirroringImage.pixelColor((x + extend) + 1, (y + extend) - 1).value();
                value_y += qMirroringImage.pixelColor((x + extend) - 1, (y + extend) + 1).value();
                value_y += qMirroringImage.pixelColor((x + extend), (y + extend) + 1).value();
                value_y += qMirroringImage.pixelColor((x + extend) + 1, (y + extend) + 1).value();

                /// Combine Image
                int sum = qAbs(value_x) + qAbs(value_y);
                sum = sum > 255 ? 255:sum;
                sum = sum < 0 ? 0 : sum;

                edgePixels[x + (y * width)] = sum;
            }

    }
    if(edge == SOBEL){
        for(int y =0; y < height; y++)
            for(int x = 0; x < width; x++){
                int value_x = 0;
                int value_y = 0;

                /// Calculate Gradient X
                value_x += qMirroringImage.pixelColor((x + extend) - 1, (y + extend) - 1).value();
                value_x -= qMirroringImage.pixelColor((x + extend) + 1, (y + extend) - 1).value();
                value_x += 2 * qMirroringImage.pixelColor((x + extend) - 1, (y + extend)).value();
                value_x -= 2 * qMirroringImage.pixelColor((x + extend) + 1, (y + extend)).value();
                value_x += qMirroringImage.pixelColor((x + extend) - 1, (y + extend) + 1).value();
                value_x -= qMirroringImage.pixelColor((x + extend) + 1, (y + extend) + 1).value();

                /// Calculate Gradient Y
                value_y -= qMirroringImage.pixelColor((x + extend) - 1, (y + extend) - 1).value();
                value_y -= 2 * qMirroringImage.pixelColor((x + extend), (y + extend) - 1).value();
                value_y -= qMirroringImage.pixelColor((x + extend) + 1, (y + extend) - 1).value();
                value_y += qMirroringImage.pixelColor((x + extend) - 1, (y + extend) + 1).value();
                value_y += 2 * qMirroringImage.pixelColor((x + extend), (y + extend) + 1).value();
                value_y += qMirroringImage.pixelColor((x + extend) + 1, (y + extend) + 1).value();

                /// Combine Image
                int sum = qAbs(value_x) + qAbs(value_y);
                sum = sum > 255 ? 255:sum;

                edgePixels[x + (y * width)] = sum;
            }

    }
    if(edge == CANNY){
        smoothing(extend); // gaussian filter of 5x5 kernel
        calculateGradient(extend); // calculate gradient in x,, gradient in y and the direction of gradient

        qNonMaximaImage = QImage(width, height, QImage::Format_Grayscale8);
        nonMaximaPixels = qNonMaximaImage.bits();

        for(int y =1; y < height - 1; y++)
            for(int x = 1; x < width - 1; x++){
                int value_x = qGradientXImage.pixelColor(x, y).value();
                int value_y = qGradientYImage.pixelColor(x, y).value();

                /// Non-maximun supression
                const float dir = (float)(fmod(atan2(value_x, value_y) + M_PI, M_PI) / M_PI) * 8;

                int gradients[3][3] = {};
                gradients[0][0] = qGradientImage.pixelColor((x) - 1, (y) - 1).value();
                gradients[0][1] = qGradientImage.pixelColor((x), (y) - 1).value();
                gradients[0][2] = qGradientImage.pixelColor((x) + 1, (y) - 1).value();
                gradients[1][0] = qGradientImage.pixelColor((x) - 1, (y)).value();
                gradients[1][1] = qGradientImage.pixelColor((x), (y)).value();
                gradients[1][2] = qGradientImage.pixelColor((x) + 1, (y)).value();
                gradients[2][0] = qGradientImage.pixelColor((x) - 1, (y) + 1).value();
                gradients[2][1] = qGradientImage.pixelColor((x), (y) + 1).value();
                gradients[2][2] = qGradientImage.pixelColor((x) + 1, (y) + 1).value();

                if (((dir <= 1 || dir > 7) &&  gradients[1][1] > gradients[1][0] && gradients[1][1] > gradients[1][2]) || // 0 deg
                   ((dir > 1 && dir <= 3) && gradients[1][1] > gradients[0][2] && gradients[1][1] > gradients[2][0]) || // 45 deg
                   ((dir > 3 && dir <= 5) && gradients[1][1] > gradients[0][1] && gradients[1][1] > gradients[2][1]) || // 90 deg
                   ((dir > 5 && dir <= 7) && gradients[1][1] > gradients[0][0] && gradients[1][1] > gradients[2][2]))   // 135 deg
                    nonMaximaPixels[x + (y * width)] = gradients[1][1];
                else
                    nonMaximaPixels[x + (y * width)] = 0;
            }
    }
}
void convert::smoothing(int extend){

    for(int y = extend; y < height-extend; y++)
        for(int x = extend; x < width-extend; x++){
            int gaussian = 0;

            gaussian += qMirroringImage.pixelColor((x) - 2, (y) - 2).value()  * gaussian5x5[0][0];
            gaussian += qMirroringImage.pixelColor((x) - 1, (y) - 2).value()  * gaussian5x5[0][1];
            gaussian += qMirroringImage.pixelColor((x), (y) - 2).value()      * gaussian5x5[0][2];
            gaussian += qMirroringImage.pixelColor((x) + 1, (y) - 2).value()  * gaussian5x5[0][3];
            gaussian += qMirroringImage.pixelColor((x) + 2, (y) - 2).value()  * gaussian5x5[0][4];

            gaussian += qMirroringImage.pixelColor((x) - 2, (y) - 1).value()  * gaussian5x5[1][0];
            gaussian += qMirroringImage.pixelColor((x) - 1, (y) - 1).value()  * gaussian5x5[1][1];
            gaussian += qMirroringImage.pixelColor((x), (y) - 1).value()      * gaussian5x5[1][2];
            gaussian += qMirroringImage.pixelColor((x) + 1, (y) - 1).value()  * gaussian5x5[1][3];
            gaussian += qMirroringImage.pixelColor((x) + 2, (y) - 1).value()  * gaussian5x5[1][4];

            gaussian += qMirroringImage.pixelColor((x) - 2, (y)).value()      * gaussian5x5[2][0];
            gaussian += qMirroringImage.pixelColor((x) - 1, (y)).value()      * gaussian5x5[2][1];
            gaussian += qMirroringImage.pixelColor((x), (y)).value()          * gaussian5x5[2][2];
            gaussian += qMirroringImage.pixelColor((x) + 1, (y)).value()      * gaussian5x5[2][3];
            gaussian += qMirroringImage.pixelColor((x) + 2, (y)).value()      * gaussian5x5[2][4];

            gaussian += qMirroringImage.pixelColor((x) - 2, (y) + 1).value()  * gaussian5x5[3][0];
            gaussian += qMirroringImage.pixelColor((x) - 1, (y) + 1).value()  * gaussian5x5[3][1];
            gaussian += qMirroringImage.pixelColor((x), (y) + 1).value()      * gaussian5x5[3][2];
            gaussian += qMirroringImage.pixelColor((x) + 1, (y) + 1).value()  * gaussian5x5[3][3];
            gaussian += qMirroringImage.pixelColor((x) + 2, (y) + 1).value()  * gaussian5x5[3][4];

            gaussian += qMirroringImage.pixelColor((x) - 2, (y) + 2).value()  * gaussian5x5[4][0];
            gaussian += qMirroringImage.pixelColor((x) - 1, (y) + 2).value()  * gaussian5x5[4][1];
            gaussian += qMirroringImage.pixelColor((x), (y) + 2).value()      * gaussian5x5[4][2];
            gaussian += qMirroringImage.pixelColor((x) + 1, (y) + 2).value()  * gaussian5x5[4][3];
            gaussian += qMirroringImage.pixelColor((x) + 2, (y) + 2).value()  * gaussian5x5[4][4];

            int gray = (int)gaussian / 159;
            qMirroringImage.setPixel(x, y, qRgb(gray, gray, gray));

        }


}

void convert::calculateGradient(int extend){

    qGradientXImage = QImage(width, height ,  QImage::Format_Grayscale8);
    qGradientYImage = QImage(width , height ,  QImage::Format_Grayscale8);
    qGradientImage  = QImage(width, height,  QImage::Format_Grayscale8);

    uchar*  gradientXPixels = qGradientXImage.bits();
    uchar*  gradientYPixels = qGradientYImage.bits();
    uchar*  gradientPixels =  qGradientImage.bits();

    for(int y = 0; y < height; y++)
        for(int x = 0; x < width; x++){
            int value_x = 0;
            int value_y = 0;

            /// Calculate Gradient X
            value_x += qMirroringImage.pixelColor((x + extend) - 1, (y + extend) - 1).value();
            value_x -= qMirroringImage.pixelColor((x + extend) + 1, (y + extend) - 1).value();
            value_x += 2 * qMirroringImage.pixelColor((x + extend) - 1, (y + extend)).value();
            value_x -= 2 * qMirroringImage.pixelColor((x + extend) + 1, (y + extend)).value();
            value_x += qMirroringImage.pixelColor((x + extend) - 1, (y + extend) + 1).value();
            value_x -= qMirroringImage.pixelColor((x + extend) + 1, (y + extend) + 1).value();

            /// Calculate Gradient Y
            value_y -= qMirroringImage.pixelColor((x + extend) - 1, (y + extend) - 1).value();
            value_y -= 2 * qMirroringImage.pixelColor((x + extend), (y + extend) - 1).value();
            value_y -= qMirroringImage.pixelColor((x + extend) + 1, (y + extend) - 1).value();
            value_y += qMirroringImage.pixelColor((x + extend) - 1, (y + extend) + 1).value();
            value_y += 2 * qMirroringImage.pixelColor((x + extend), (y + extend) + 1).value();
            value_y += qMirroringImage.pixelColor((x + extend) + 1, (y + extend) + 1).value();

            gradientXPixels[x + (y * width)] = value_x;
            gradientYPixels[x + (y * width)] = value_y;

            gradientPixels[x + (y * width)] = qSqrt(qPow(value_x, 2) + qPow(value_y, 2));
        }


}

void convert::showHistogram(int* equalizedHistogram){

    setEqualized = new QBarSet("Intensity Pixels");

    for(int i = 0; i < 256; i++){
        categoriesEqualized.append("" + i);
        setEqualized->insert(i, equalizedHistogram[i]);
    }

    seriesEqualized = new QBarSeries();
    seriesEqualized->append(setEqualized);

    chartEqualized = new QChart();
    chartEqualized->addSeries(seriesEqualized);
    chartEqualized->setTitle("Histogram");
    chartEqualized->setAnimationOptions(QChart::SeriesAnimations);

    axisEqualized = new QBarCategoryAxis();
    axisEqualized->append(categoriesEqualized);
    chartEqualized->createDefaultAxes();

    chartViewEqualized = new QChartView(chartEqualized );
    chartViewEqualized->setRenderHint(QPainter::Antialiasing);

    windowEqualized.setCentralWidget(chartViewEqualized);
    windowEqualized.resize(420, 300);
    windowEqualized.show();

}

void convert::RGB_HSL(QImage imagehsl){

    imageHSL = QImage(imagehsl.width(), imagehsl.height(), imagehsl.format());
    imageRed = QImage(imagehsl.width(), imagehsl.height(), imagehsl.format());
    imageGreen = QImage(imagehsl.width(), imagehsl.height(), imagehsl.format());
    imageBlue = QImage(imagehsl.width(), imagehsl.height(), imagehsl.format());

    for(int y = 0; y < imagehsl.height(); y++){
       for(int x = 0; x < imagehsl.width(); x++){
           QColor color(imagehsl.pixel(x, y));

           qreal var_R = (qreal)color.red()/255.0f;
           qreal var_G = (qreal)color.green()/255.0f;
           qreal var_B = (qreal)color.blue()/255.0f;

           qreal var_Min = qMin(var_R,qMin(var_G,var_B));
           qreal var_Max = qMax(var_R,qMax(var_G,var_B));
           qreal del_Max = var_Max - var_Min;

           qreal H = 0;
           qreal S = 0;
           qreal L = (var_Max + var_Min)/2;

           if(del_Max ==0){
               H = 0;
               S = 0;
           }else{
               if(L < 0.5){S = del_Max/(var_Max + var_Min);}
               else{S = del_Max/(2- var_Max -var_Min);}

               qreal del_R =(((var_Max - var_R) / 6) + (del_Max / 2)) / del_Max;
               qreal del_G =(((var_Max - var_G) / 6) + (del_Max / 2)) / del_Max;
               qreal del_B =(((var_Max - var_B) / 6) + (del_Max / 2)) / del_Max;

               if(var_R == var_Max){H = (del_B - del_G);}
               else if(var_G == var_Max){H = ((1/3.0f) + del_R - del_B);}
               else if(var_B == var_Max){H = ((2/3.0f) + del_G - del_R);}

               if(H < 0){ H+=1;}
               if(H > 1){ H-=1;}
           }
           QColor colorHsl;
           colorHsl.setHslF(H,S,L);
           imageHSL.setPixelColor(x,y,colorHsl);
           int r = H*255;
           int g = S*255;
           int b = L*255;

           imageRed.setPixel(x,y,qRgb(r,0,0));
           imageGreen.setPixel(x,y,qRgb(0,g,0));
           imageBlue.setPixel(x,y,qRgb(0,0,b));

       }
    }
}

void convert::RGB_HSV(QImage imagehsv){

    imageHSV = QImage(imagehsv.width(), imagehsv.height(), imagehsv.format());
    imageRed = QImage(imagehsv.width(), imagehsv.height(), imagehsv.format());
    imageGreen = QImage(imagehsv.width(), imagehsv.height(), imagehsv.format());
    imageBlue = QImage(imagehsv.width(), imagehsv.height(), imagehsv.format());

    for(int y = 0; y < imagehsv.height(); y++){
       for(int x = 0; x < imagehsv.width(); x++){
           QColor colorhsv(imagehsv.pixel(x, y));

           qreal var_R = (qreal)colorhsv.red()/255.0f;
           qreal var_G = (qreal)colorhsv.green()/255.0f;
           qreal var_B = (qreal)colorhsv.blue()/255.0f;

           qreal var_Min = qMin(var_R,qMin(var_G,var_B));
           qreal var_Max = qMax(var_R,qMax(var_G,var_B));
           qreal del_Max = var_Max - var_Min;

           qreal H = 0;
           qreal S = 0;
           qreal V = var_Max;

           if(del_Max ==0){
               H = 0;
               S = 0;
           }else{

               S = (del_Max / var_Max);

               qreal del_R =(((var_Max - var_R) / 6) + (del_Max / 2)) / del_Max;
               qreal del_G =(((var_Max - var_G) / 6) + (del_Max / 2)) / del_Max;
               qreal del_B =(((var_Max - var_B) / 6) + (del_Max / 2)) / del_Max;

               if(var_R == var_Max){H = (del_B - del_G);}
               else if(var_G == var_Max){H = ((1/3.0f) + del_R - del_B);}
               else if(var_B == var_Max){H = ((2/3.0f) + del_G - del_R);}

               if(H < 0){ H+=1;}
               if(H > 1){ H-=1;}
           }
           QColor colorHsv;           
           colorHsv.setHsvF(H,S,V);
           imageHSV.setPixelColor(x,y,colorHsv);
           int r = H*255;
           int g = S*255;
           int b = V*255;

           imageRed.setPixel(x,y,qRgb(r,0,0));
           imageGreen.setPixel(x,y,qRgb(0,g,0));
           imageBlue.setPixel(x,y,qRgb(0,0,b));

       }
    }
}

void convert::channelsRGB(QImage ImageRGB){
    imageRed = QImage(ImageRGB.width(), ImageRGB.height(),ImageRGB.format());
    imageGreen = QImage(ImageRGB.width(), ImageRGB.height(),ImageRGB.format());
    imageBlue = QImage(ImageRGB.width(), ImageRGB.height(),ImageRGB.format());

    for(int y = 0; y < ImageRGB.height(); y++){
       for(int x = 0; x < ImageRGB.width(); x++){
          QColor color(ImageRGB.pixel(x, y));
          imageRed.setPixel(x, y, qRgb(color.red(),0,0));
          imageGreen.setPixel(x, y, qRgb(0, color.green(),0));
          imageBlue.setPixel(x, y, qRgb(0,0,color.blue()));
       }
    }
}

void convert::calculateHistogram(){

    //printf("calculate Histogram\n");
    int imageSize = width * height;

    for(int pixel = 0; pixel < imageSize; pixel++){
        int value = (int)originalPixels[pixel];
        histogram[value]++;
    }

    //printf("END calculate Histogram\n");

}

void convert::calculateAccumulatedFrequency(){
    accumulatedFrequency[0] = histogram[0];
    for(int frequency = 1; frequency < 256; frequency++){
        accumulatedFrequency[frequency] = histogram[frequency] + accumulatedFrequency[frequency - 1];
    }
}



void convert::equalizeHistogram(){
    qEqualizedImage = QImage(width, height, QImage::Format_Grayscale8);
    uchar* equalizedPixels = qEqualizedImage.bits();

    calculateHistogram();
    calculateAccumulatedFrequency();
   // printf("calculate Accumulated Histogram\n");

    float scale = 255.0f/accumulatedFrequency[255];
    for(int value = 0; value < 256; value++){

        newValues[value] = (scale * accumulatedFrequency[value]);
    }
    int imageSize = width * height;
    for(int pixel = 0; pixel < imageSize; pixel++){
        int value = (int)newValues[originalPixels[pixel]];
        equalizedPixels[pixel] = value;
        equalizedHistogram[value]++;
    }
}

int convert::calculatedThreshold(ThresMethod thres){
    int threshold = 0;

    if(thres == ISODATA){
        calculateHistogram();
        calculateAccumulatedFrequency();
        int oldT = 0, newT = 128; // T = 2^B-1

        while(oldT != newT){
            oldT = newT;
            float meanF = 0.0f;
            float meanB = 0.0f;

            for(int f=0; f < newT; f++){
                meanF += f * histogram[f];
            }
            meanF = meanF/accumulatedFrequency[newT - 1];

            for(int b = newT; b < 256; b++){
                meanB += b * histogram[b];
            }
            meanB = meanB / (accumulatedFrequency[255] - accumulatedFrequency[newT - 1]);
            newT = (meanF + meanB)/2;
        }
        return newT;
    }
    else if(thres == OTSU){
        calculateHistogram();
        calculateAccumulatedFrequency();

        int imageSize = width * height;
        int sum = 0;
        int q1 = 0; // class 1
        int q2 = 0; // class 2
        float meanF = 0.0;
        float max = 0;  // maximun -- threshold

        for (int i = 0; i < 256; i++){
          sum += i * ((int)histogram[i]);
        }

        for (int i = 0 ; i <256 ; i++) {

            q1 += histogram[i];
            q2 = imageSize - q1;

            meanF += (float) (i * ((int)histogram[i]));
            float m1 = meanF / q1;
            float m2 = (sum - meanF) / q2;

            float temp = (float) q1 * (float) q2 * (m1 - m2) * (m1 - m2);

            // Update the threshold if necessary
            if (temp > max) {
              max = temp;
              threshold = i;
            }
        }

        return threshold;
    }
    else
        printf("Threshold method not valid");

    return threshold;

}

void convert::thresholding(int threshold){
    qTresHoldedImage = QImage(width, height, QImage::Format_Grayscale8);
    uchar* thresholdedPixels = qTresHoldedImage.bits();

    int imageSize = width * height;
    for(int pixel = 0;  pixel < imageSize; pixel++){
        thresholdedPixels[pixel] = originalPixels[pixel] < threshold ? 0:255;
    }
}

void convert::smothing(Filters filter){
    qDenoisingImage = QImage(width, height, QImage::Format_Grayscale8);
    originalPixels = qDenoisingImage.bits();


    if(filter == MEAN){
        for(int y=1; y< height-1; y++)
            for(int x=1; x< width-1; x++){
                int average = 0;
                average += qOriginalImage.pixelColor((x-1), (y-1)).value();
                average += qOriginalImage.pixelColor(x, (y-1)).value();
                average += qOriginalImage.pixelColor((x+1), (y-1)).value();
                average += qOriginalImage.pixelColor((x-1), y).value();
                average += qOriginalImage.pixelColor(x, y).value();
                average += qOriginalImage.pixelColor((x+1), y).value();
                average += qOriginalImage.pixelColor((x-1), (y+1)).value();
                average += qOriginalImage.pixelColor(x, (y+1)).value();
                average += qOriginalImage.pixelColor((x+1),(y+1)).value();

                originalPixels[x +(y*width)] = (int)average/9;
            }
    }else if(filter == MEDIAN){
        for(int y=1; y< height-1; y++)
            for(int x=1; x< width-1; x++){
                QVector<int> block;
                block.append(qOriginalImage.pixelColor((x-1), (y-1)).value());
                block.append(qOriginalImage.pixelColor(x, (y-1)).value());
                block.append(qOriginalImage.pixelColor((x+1), (y-1)).value());
                block.append(qOriginalImage.pixelColor((x-1), y).value());
                block.append(qOriginalImage.pixelColor(x, y).value());
                block.append(qOriginalImage.pixelColor((x+1), y).value());
                block.append(qOriginalImage.pixelColor((x-1), (y+1)).value());
                block.append(qOriginalImage.pixelColor(x, (y+1)).value());
                block.append(qOriginalImage.pixelColor((x+1), (y+1)).value());

                qSort(block);

                originalPixels[x + (y*width)] = block.at(5);
            }
    }
    if(filter == GAUSSIAN){
        for(int y=1; y<height-1; y++)
            for(int x=1; x<width-1; x++){
                int gaussian = 0;
                gaussian += qOriginalImage.pixelColor((x-1), (y-1)).value() * gaussian3x3[0][0];
                gaussian += qOriginalImage.pixelColor(x, (y-1)).value() * gaussian3x3[1][0];
                gaussian += qOriginalImage.pixelColor((x+1), (y-1)).value() * gaussian3x3[2][0];
                gaussian += qOriginalImage.pixelColor((x-1), y).value() * gaussian3x3[0][1];
                gaussian += qOriginalImage.pixelColor(x, y).value() * gaussian3x3[1][1];
                gaussian += qOriginalImage.pixelColor((x+1), y).value() * gaussian3x3[2][1];
                gaussian += qOriginalImage.pixelColor((x-1), (y+1)).value() * gaussian3x3[0][2];
                gaussian += qOriginalImage.pixelColor(x, (y+1)).value() * gaussian3x3[1][2];
                gaussian += qOriginalImage.pixelColor((x+1), (y+1)).value() * gaussian3x3[2][2];

                originalPixels[x +(y*width)] = (int)gaussian/16;
            }
    }

}
void convert::subSumpling(int value){
    int subWidth = width/value;
    int subHeight = height/value;

    QSampledImage = QImage(subWidth, subHeight, QImage::Format_Grayscale8);

    for(int subY=0; subY<subHeight; subY++){
        for(int subX=0; subX<subWidth; subX++){
            int average = 0;
            int initX = (value*subX);
            int initY = (value*subY);
            for(int y=0; y<value; y++)
                for(int x=0; x<value; x++){
                    average += originalPixels[(initX + x)+(initY + y)*width];
                }
            int gray = average/(value*value);
            QSampledImage.setPixel(subX, subY, qRgb(gray, gray, gray));
        }
    }
}

void convert::bitDepthReduction(int bitsToReduce){
    qBitDepthReducedImage = QImage(width, height, QImage::Format_Grayscale8);
    uchar* reducedPixels = qBitDepthReducedImage.bits();

    if(bitsToReduce > 8){
        printf("Error in bit Depth reduction: valur upper to ...");
        exit(EXIT_FAILURE);
    }
    int numberColors = 1 << bitsToReduce;
    int numberGrayScale = 256/numberColors;
    int maxValue = 255/numberGrayScale;

    for(int pixel = 0; pixel<(width*height);pixel++){
        reducedPixels[pixel]=((originalPixels[pixel]/numberGrayScale)*255)/maxValue;
    }
}

void convert::binaryErosion(OperatorsMorph operatorMorph){
    qMorphologicalImage = QImage(width, height, QImage::Format_Grayscale8);
    morphologicalPixels = qMorphologicalImage.bits();
    int kernel_width = 7;
    int kernel_height = 7;
    int stride_x = 3;
    int stride_y = 3;
    int kernel[49] = {0,0,1,1,1,0,0,
                      0,1,1,1,1,1,0,
                      1,1,1,1,1,1,1,
                      1,1,1,1,1,1,1,
                      1,1,1,1,1,1,1,
                      0,1,1,1,1,1,0,
                      0,0,1,1,1,0,0};
    int sizeBlock = kernel_width * kernel_height;
    uchar* block = (uchar*)malloc(sizeBlock * sizeof(uchar));
    if(operatorMorph == EROSION){
        for(int y = 3; y < height-3; y++)
            for(int x = 3; x < width-3; x++){

                for(int kernel_y = 0; kernel_y < kernel_height; kernel_y++)
                    for(int kernel_x = 0; kernel_x < kernel_width; kernel_x++){
                        block[kernel_x + (kernel_y * kernel_width)] = qTresHoldedImage.pixelColor((x - stride_x) + kernel_x, (y - stride_y) + kernel_y).value();
                    }
                bool containsAll = true;
                for(int i = 0; i < sizeBlock; i++){
                    if(kernel[i] == 1 && (int)block[i] == 0){
                        containsAll = false;
                        break;
                    }
                }
                morphologicalPixels[x + (y * width)] = (containsAll == true) ? 255: 0;
            }
        free(block);
    }
    if(operatorMorph == DILATION){
        for(int y = 3; y < height-3; y++)
            for(int x = 3; x < width-3; x++){

                for(int kernel_y = 0; kernel_y < kernel_height; kernel_y++)
                    for(int kernel_x = 0; kernel_x < kernel_width; kernel_x++){
                        block[kernel_x + (kernel_y * kernel_width)] = qTresHoldedImage.pixelColor((x - stride_x) + kernel_x, (y - stride_y) + kernel_y).value();
                    }
                bool containsOne = false;
                for(int i = 0; i < sizeBlock; i++){
                    if(kernel[i] == 1 && (int)block[i] == 255){
                        containsOne = true;
                        break;
                    }
                }
                morphologicalPixels[x + (y * width)] = (containsOne == true) ? 255: 0;
            }
        free(block);
    }
    if(operatorMorph == GRAYF_EROSION){
        for(int y = 3; y < height-3; y++)
            for(int x = 3; x < width-3; x++){

                for(int kernel_y = 0; kernel_y < kernel_height; kernel_y++)
                    for(int kernel_x = 0; kernel_x < kernel_width; kernel_x++){
                        block[kernel_x + (kernel_y * kernel_width)] = qOriginalImage.pixelColor((x - stride_x) + kernel_x, (y - stride_y) + kernel_y).value();
                    }
                int min = block[0];
                for(int i = 0; i < sizeBlock; i++){
                    if(block[i] < min && kernel[i]){
                        min = block[i];
                    }
                }
                morphologicalPixels[x + (y * width)] = min;
            }
        free(block);
    }
    if(operatorMorph == GRAYF_DILATION){
        for(int y = 3; y < height-3; y++)
            for(int x = 3; x < width-3; x++){

                for(int kernel_y = 0; kernel_y < kernel_height; kernel_y++)
                    for(int kernel_x = 0; kernel_x < kernel_width; kernel_x++){
                        block[kernel_x + (kernel_y * kernel_width)] = qOriginalImage.pixelColor((x - stride_x) + kernel_x, (y - stride_y) + kernel_y).value();
                    }
                int max = block[0];
                for(int i = 0; i < sizeBlock; i++){
                    if(block[i] > max && kernel[i]){
                        max = block[i];
                    }
                }
                morphologicalPixels[x + (y * width)] = max;
            }
        free(block);
    }
    if(operatorMorph == GRAYNF_EROSION){
        for(int y = 3; y < height-3; y++)
            for(int x = 3; x < width-3; x++){

                for(int kernel_y = 0; kernel_y < kernel_height; kernel_y++)
                    for(int kernel_x = 0; kernel_x < kernel_width; kernel_x++){
                        block[kernel_x + (kernel_y * kernel_width)] = qOriginalImage.pixelColor((x - stride_x) + kernel_x, (y - stride_y) + kernel_y).value() + kernel_x + kernel_y;
                    }
                int min = block[0];
                for(int i = 0; i < sizeBlock; i++){
                    if(block[i] < min){
                        min = block[i];
                    }
                }
                morphologicalPixels[x + (y * width)] = min;
            }
        free(block);
    }
    if(operatorMorph == GRAYNF_DILATION){
        for(int y = 3; y < height-3; y++)
            for(int x = 3; x < width-3; x++){

                for(int kernel_y = 0; kernel_y < kernel_height; kernel_y++)
                    for(int kernel_x = 0; kernel_x < kernel_width; kernel_x++){
                        block[kernel_x + (kernel_y * kernel_width)] = qOriginalImage.pixelColor((x - stride_x) + kernel_x, (y - stride_y) + kernel_y).value()- kernel_x - kernel_y;
                    }
                int max = block[0];
                for(int i = 0; i < sizeBlock; i++){
                    if(block[i] > max){
                        max = block[i];
                    }
                }
                morphologicalPixels[x + (y * width)] = max;
            }
        free(block);
    }
    if(operatorMorph == GRADIENT){
        for(int y = 3; y < height-3; y++)
            for(int x = 3; x < width-3; x++){

                for(int kernel_y = 0; kernel_y < kernel_height; kernel_y++)
                    for(int kernel_x = 0; kernel_x < kernel_width; kernel_x++){
                        block[kernel_x + (kernel_y * kernel_width)] = qOriginalImage.pixelColor((x - stride_x) + kernel_x, (y - stride_y) + kernel_y).value();
                    }
                int min = block[0];
                int max = block[0];
                for(int i = 0; i < sizeBlock; i++){
                    if(block[i] > max && kernel[i]){
                        max = block[i];
                    }
                    if(block[i] < min && kernel[i]){
                        min = block[i];
                    }
                }
                morphologicalPixels[x + (y * width)] = (max-min);
            }
        free(block);
    }
}




