#ifndef DIALOGVIEWIMAGE_H
#define DIALOGVIEWIMAGE_H

#include <QDialog>

namespace Ui {
class DialogViewImage;
}

class DialogViewImage : public QDialog
{
    Q_OBJECT

public:
    explicit DialogViewImage(QWidget *parent = 0);
    ~DialogViewImage();

private:
    Ui::DialogViewImage *ui;
};

#endif // DIALOGVIEWIMAGE_H
