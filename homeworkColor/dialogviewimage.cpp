#include "dialogviewimage.h"
#include "ui_dialogviewimage.h"
#include "convert.h"

DialogViewImage::DialogViewImage(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogViewImage)
{
    ui->setupUi(this);
}

DialogViewImage::~DialogViewImage()
{
    delete ui;
    convert* reader = new convert;
    QImage imageRed;
    imageRed = reader->getQOriginalImageRed();
    ui->labelImage->setPixmap(QPixmap::fromImage(imageRed));
}
