#ifndef HOMEWORKCOLOR_H
#define HOMEWORKCOLOR_H

#include <QMainWindow>
#include "ui_homeworkcolor.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QGraphicsScene>
#include <QGraphicsview>
#include <QGraphicsPixmapItem>
#include <QImageReader>
#include <QtCharts>
#include <QMainWindow>
#include <cstdlib>
#include <QImage>
#include <iostream>
#include <QString>
#include <cmath>
#include "convert.h"

namespace Ui {
class homeworkColor;
}

class homeworkColor : public QMainWindow
{
    Q_OBJECT

public:
    explicit homeworkColor(QWidget *parent = 0);
    ~homeworkColor();

private slots:
    void on_loadImage_clicked();

    void on_radioButtonRGB_HSL_clicked();

    void on_radioButtonRGB_HSV_clicked();

    void on_radioButtonRGB_clicked();

    void on_radioButtonOtsu_clicked();

    void on_radioButtonIsodata_clicked();

    void on_radioButtonEqualized_clicked();

    void on_radioButtonHistogram_clicked();

    void on_radioButtonMean_clicked();

    void on_radioButtonMedian_clicked();

    void on_radioButtonGaussian_clicked();

    void on_radioButtonSampling_clicked();

    void on_radioButtonQuantization_clicked();

    void on_gradientButton_clicked();

    void on_sobelButton_clicked();

    void on_prewittButton_clicked();

    void on_radioButtonErosion_clicked();

    void on_radioButtonDilation_clicked();

    void on_radioButtonErosionGrayScale_clicked();

    void on_radioButtonDilationGrayScale_clicked();

    void on_radioButtonErosionNonFlat_clicked();

    void on_radioButtonDilationNonFlat_clicked();

    void on_radioButtonGradientGrayScale_clicked();

    void on_cannyButton_clicked();

    void on_radioButtonOpening_clicked();

    void on_radioButtonClosing_clicked();

    void on_radioButtonOpeningGray_clicked();

    void on_radioButtonClosingGray_clicked();

private:
    QString filename;
    Ui::homeworkColor *ui;    
    QImage qOriginalImage;
    uchar* originalPixels;
    int width;
    int height;
};

#endif // HOMEWORKCOLOR_H
