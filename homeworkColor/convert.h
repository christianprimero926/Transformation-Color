#ifndef CONVERT_H
#define CONVERT_H
#include <iostream>
#include <QImage>
#include <QString>
#include <QColor>
#include <QImageReader>
#include <QtMath>
#include <QImageWriter>
#include <QGraphicsScene>
#include <QGraphicsview>
#include <QtCharts>
#include <QGraphicsPixmapItem>

enum ThresMethod{
    ISODATA,
    OTSU
};
enum Filters{
    MEAN,
    MEDIAN,
    GAUSSIAN
};
enum Algorithms{
    BASIC,
    SOBEL,
    PREWITT,
    CANNY
};
enum OperatorsMorph{
    EROSION,
    DILATION,
    GRAYF_EROSION,
    GRAYF_DILATION,
    GRAYNF_DILATION,
    GRAYNF_EROSION,
    GRADIENT
};
class convert{
private:
    QImage qOriginalImage;
    QImage qEqualizedImage;
    QImage qTresHoldedImage;
    QImage qDenoisingImage;
    QImage imageHSL;
    QImage imageHSV;
    QImage imageRed;
    QImage imageGreen;
    QImage imageBlue;
    QImage QSampledImage;
    QImage qBitDepthReducedImage;
    QImage qEdgeImage;
    QImage qGradientXImage;
    QImage qGradientYImage;
    QImage qGradientImage;
    QImage qMirroringImage;
    QImage qCalculateGradientImage;
    QImage qMorphologicalImage;
    QImage qNonMaximaImage;
    uchar* morphologicalPixels;
    uchar* originalPixels;
    uchar* edgePixels;
    uchar* denoisingPixels;
    uchar* mirroringPixels;
    uchar* nonMaximaPixels;
    int histogram[256] = {};
    int accumulatedFrequency[256]={};
    int newValues[256]={};
    int equalizedHistogram[256]={};
    QGraphicsScene* scene;
    QGraphicsView* view;
    QGraphicsPixmapItem* item;
    QBarSet* setEqualized;
    QStringList categoriesEqualized;
    QBarSeries* seriesEqualized;
    QChart* chartEqualized;
    QBarCategoryAxis* axisEqualized;
    QChartView* chartViewEqualized;
    QMainWindow windowEqualized;
    int element;
    int width;
    int height;
    int bitDepth;
    int format;    
public:
    convert();    
    void loadImage(QString filename);    
    void RGB_HSL(QImage imagehsl);
    void RGB_HSV(QImage imagehsv);
    void channelsRGB(QImage Image);
    void showImage(QImage image);
    void showHistogram(int* equalizedHistogram);
    void calculateHistogram();
    void calculateAccumulatedFrequency();
    void equalizeHistogram();
    void thresholding(int threshold);
    void smothing(Filters filter);
    void subSumpling(int value);
    void bitDepthReduction(int bitsToReduce);
    void smoothing(int extend);
    void calculateGradient(int extend);
    void hysteresis(int t1, int t2);
    void mirroring(int extend);
    void edgeDetection(Algorithms alg, int extend);
    void smothing();
    void calculateGradient();
    void binaryErosion(OperatorsMorph operatorMorph);
    void writeImage(QImage imageToSave, QString imageNameOut);

    QImage getQOriginalImage();
    QImage getQOriginalImageRed();
    QImage getQOriginalImageGreen();
    QImage getQOriginalImageBlue();
    QImage getQEqualizedImage();
    QImage getQThresholdedImage();
    QImage getImageHSL();
    QImage getImageHSV();
    QImage getDenoisingImage();
    QImage getSampledImage();
    QImage getqBitDepthReducedImage();
    QImage getMirroringImage();
    QImage getGradientImage();
    QImage getGradientXImage();
    QImage getGradientYImage();
    QImage getEdgeImage();
    QImage getQNonMaximaImage();
    QImage getqCalculateGradientImage();
    QImage getQMorphologicalImage();

    int calculatedThreshold(ThresMethod thres);

    int* getHistogram();
    int* getEqualizedHistogram();
    int* getAccumulatedFrequency();
};

#endif // CONVERT_H
